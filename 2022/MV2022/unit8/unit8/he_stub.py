# conda install scipy

import glob
import cv2
import numpy as np
import cvHelper
from scipy.spatial.transform import Rotation as R

basedir = "data1"

# read in hand positions
poseFiles=glob.glob(f"{basedir}/poses/*.txt")
poseFiles.sort()

gripperRvecs=[]
gripperTvecs=[]
for p in poseFiles:
    m=np.loadtxt(p)
    gripperRvecs.append(m[0:3, 0:3])
    tvec = m[0:3,3]
    gripperTvecs.append(tvec.T/1000) # division factor ok?

imgFiles=glob.glob(f"{basedir}/imgs/*.png")
imgFiles.sort()

# camera positions
imgRvecs,imgTvecs = [],[]

#for file in imgFiles:
#...
