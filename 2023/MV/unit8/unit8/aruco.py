#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
from cv2 import aruco
import cvHelper
import numpy as np

img = cv2.imread('aruco_markers.jpg',0)

distCoeffs = np.array([ 0.307 , -4.5408, -0.0119, -0.0153, 37.5154])

cameraMatrix = np.array([[7235.7109, 0. , 1832.7629],
                [ 0. , 7223.2731, 1248.6915],
                [ 0. , 0. , 1. ]])

aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)

parameters =  aruco.DetectorParameters_create()

corners, ids, _ = aruco.detectMarkers(img, aruco_dict, 
                                      parameters=parameters)

rvec, tvec, markerPoints = aruco.estimatePoseSingleMarkers(corners, 0.02, 
                                                           cameraMatrix,
                                                           distCoeffs)

imgc1 = cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)

assert(rvec.shape == tvec.shape)
for i in range(rvec.shape[0]):
    aruco.drawAxis(imgc1, cameraMatrix, distCoeffs, rvec[i], tvec[i], 0.01)  # Draw axis

imgc = aruco.drawDetectedMarkers(cv2.cvtColor(img,cv2.COLOR_GRAY2RGB), 
                                 corners, ids)
cvHelper.image(imgc1)

