#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Gernot Stübl
# Version 1.96


#import cv2
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D 
import cv2



################################
#adopted from https://stackoverflow.com/questions/5849800/tic-toc-functions-analog-in-python#5849861
import time
def TicTocGenerator():
    # Generator that returns time differences
    ti = 0           # initial time
    tf = time.time() # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf-ti # returns the time difference

TicToc = TicTocGenerator() # create an instance of the TicTocGen generator

# This will be the main function through which we define both tic() and toc()
def toc(tempBool=True):
    # Prints the time difference yielded by generator instance TicToc
    tempTimeInterval = next(TicToc)
    if tempBool:
        print( "Elapsed time: %f seconds.\n" %tempTimeInterval )

def tic():
    # Records a time in TicToc, marks the beginning of a time interval
    toc(False)
#####################################

def image(gray_img, title=None):
    # for double images
    vvmin=0 
    vvmax=1 
  
    # for uint8 images
    if hasattr(gray_img,'dtype') and gray_img.dtype == np.uint8:  
        vvmin=0
        vvmax=255
    plt.figure()  
    if title:
        plt.title(title)
    plt.imshow(gray_img, cmap='gray', vmin=vvmin, vmax=vvmax)
    plt.colorbar()
    plt.show()

def imagesc(img, title=None):
    plt.figure()
    if title:
        plt.title(title)
    plt.imshow(img,  vmin=np.min(img), vmax=np.max(img), cmap='hot')
    plt.colorbar() 
    plt.show()  # display it
    
def imhist(gray_img):
    #hist = cv2.calcHist([gray_img],[0],None,[256],[0,256])
    plt.figure()
    plt.hist(gray_img.ravel(),256,[0,256])
    plt.title('Histogram for gray scale image')
    plt.show()

def imHough(h, theta, rho, aspectRatio=1/5):
    # show accumlator cells

    from matplotlib import cm
    plt.figure()
    plt.imshow(np.log(1 + h),
                extent=[np.rad2deg(theta[-1]), np.rad2deg(theta[0]), rho[-1], rho[0]],
                cmap=cm.gray, aspect=aspectRatio)
    plt.xlabel('Angles (degrees)')
    plt.ylabel('Distance (pixels)')
    plt.title('Hough accumlator cells')

def displayInliers(img, B):
    img_c = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    B[:,[0, 1]] = B[:,[1, 0]]
    img3=cv2.polylines(img_c, [B], True, (0,255,255), 3)
    imagesc(img3)
    
def displayHoughPLines(img, linesP):
    cdst = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
    if linesP is not None:
        for i in range(0, len(linesP)):
            l = linesP[i][0]
            cv.line(cdst, (l[0], l[1]), (l[2], l[3]), (0,255,255), 3, cv.LINE_AA)
    imagesc(cdst)

def plot3d(img, width=100):
    print("plot3d: use '%matplotlib auto' for interactive mode")
    ratio = img.shape[0] /img.shape[1]
    dim = (width, int(ratio*width))
    # if image is too large resize it
    if np.max(img.shape) > 500:
        resized = cv2.resize(img, dim, interpolation = cv2.INTER_CUBIC)
    else:
        resized = img
    # create the x and y coordinate arrays (here we just use pixel indices)
    xx, yy = np.mgrid[0:resized.shape[0], 0:resized.shape[1]]
    
    # create the figure
    fig = plt.figure()
   
    ax = Axes3D(fig)
    #ax = fig.gca(projection='3d')
    ax.plot_surface(xx, yy, resized ,rstride=1, cstride=1, cmap='hot', linewidth=0)
    ax.xaxis.set_rotate_label(False) 
    ax.yaxis.set_rotate_label(False) 
    ax.zaxis.set_rotate_label(False) 
    # show it
    plt.show()

def showRectangle(img, point, height, width):
    displayimage = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)       
    cv2.rectangle(displayimage, point, (point[0] + width, 
                                point[1] + height), (0,255,255), 2) 
    image(displayimage)    
    
def closeFigures():
    plt.close("all")
    
def drawAxis(img, rvec, tvec, K, dist, length=3, linewidth=20, flipZ=False):   
    xlength=length
    ylength=length
    zlength=length
    if flipZ:
        zlength=-1*zlength
    points = np.float32([[0, 0, 0], 
                         [xlength, 0, 0], 
                         [0, ylength, 0], 
                         [0, 0, zlength]]).reshape(-1, 3)
    axisPoints, _ = cv2.projectPoints(points, rvec, tvec, K, dist)
   
    img = cv2.line(img, tuple(axisPoints[0].astype(int).ravel()), tuple(axisPoints[1].astype(int).ravel()), (255,0,0), linewidth)
    img = cv2.line(img, tuple(axisPoints[0].astype(int).ravel()), tuple(axisPoints[2].astype(int).ravel()), (0,255,0), linewidth)
    img = cv2.line(img, tuple(axisPoints[0].astype(int).ravel()), tuple(axisPoints[3].astype(int).ravel()), (0,0,255), linewidth)
    return img
    
    
    
